### Concurrency Module

Generally, concurrency is a painful subject.      
It involves many edge cases and gotchas that lead to voodoo bugs  
That are difficult to reproduce and to analyze in a sane matter.

OSF Concurrency module alleviates most of these pains, allowing you, the developper  
To focus on the business logic, while benefiting of smart thread-management provided  
By the OSF infrastructure.

<u>*Some Key Benefits*</u>

1. **Smart Thread Reuse**  
   When submitting a task to be executed in parallel, the infrastructure shall detect an available OSF thread executor and reuse it, instead of blindly opening a new thread for each concurrent task. (this saves both cpu and memory resources)

2. **Automatic Cleanup of Idle Thread-Executors**  
   Threads that are not in use more than a configurable time duration are automatically purged, which saves system resources.

3. **Automatic Java-Thread Decoration**  
   Running threads automatically receive the name of submitted tasks, which allows for simpler and more informative debugging process.

4. **Built-In Success/Failure Callbacks**  
   Remember those silent voodoo thread crushes? Forget about them.

All these and more to come soon...

<u>*Usage Example*</u>

```java
FruitTask fruitTask=new FruitTask("Fruit-Task");
        osfConcurrency.runConcurrently(fruitTask);
```

```java
package trainer.sandboxes.samples.tasks;

import osf.modules.concurrency.structures.OSFTask;

public class FruitTask extends OSFTask {
    public FruitTask(String name) {
        super(name);
    }

    @Override
    public void onSuccess() {
        log.info("[SUCCESS] {}", this.getClass().getSimpleName());

    }

    @Override
    public void onFailure(Throwable cause) {
        log.error("[FAILURE] {}", this.getClass().getSimpleName(), cause);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            log.debug("Fruit: " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.warn("Interrupted on iteration {} out of 100", i);
                return;
            }
        }
    }
}
```
