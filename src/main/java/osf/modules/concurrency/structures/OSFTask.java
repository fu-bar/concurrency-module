package osf.modules.concurrency.structures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.shared.exceptions.OsfException;

import java.time.Instant;
import java.util.function.Consumer;

public abstract class OSFTask implements Runnable {
    private final String name;
    protected final Logger log;
    private OSFTaskPriority priority;
    private ExecutionState executionState;
    private Consumer<Throwable> exceptionHandler;
    private boolean isRequiredToStop = false;
    private Instant startTime;
    private Instant finishTime;

    protected OSFTask(String name) {
        this.name = name;
        this.log = LogManager.getLogger(this.name);
        this.executionState = ExecutionState.AWAITING;
        this.log.debug("Task {} is awaiting execution", this.name);
        this.priority = OSFTaskPriority.NORM_PRIORITY;
    }

    public abstract void onSuccess();

    public abstract void onFailure(Throwable cause);

    public String getName() {
        return name;
    }

    public void setExceptionHandler(Consumer<Throwable> exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    public OSFTaskPriority getPriority() {
        return priority;
    }

    public void setPriority(OSFTaskPriority priority) {
        this.priority = priority;
    }

    public ExecutionState getExecutionState() {
        return executionState;
    }

    public boolean isRequiredToStop() {
        return isRequiredToStop;
    }

    public void stop() {
        log.debug("Marking {} as required to stop", name);
        this.isRequiredToStop = true;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public Instant getFinishTime() {
        return finishTime;
    }

    public void execute() {
        if (this.exceptionHandler == null) {
            log.warn("Exception handler not set for task: {}", name);
        }
        try {
            this.executionState = ExecutionState.RUNNING;
            this.startTime = Instant.now();
            this.run();
            this.executionState = ExecutionState.FINISHED;
            this.finishTime = Instant.now();
            this.onSuccess();
        } catch (Throwable throwable) {
            this.executionState = ExecutionState.FINISHED;
            this.finishTime = Instant.now();
            this.onFailure(throwable);
            log.error("Task {} has thrown an exception", name, throwable);
            if (this.exceptionHandler != null) {
                this.exceptionHandler.accept(throwable);
            } else {
                throw new OsfException("Unhandled exception in task: " + name, throwable);
            }
        }
    }
}
