package osf.modules.concurrency.structures;

public enum OSFTaskPriority {
    MIN_PRIORITY,
    NORM_PRIORITY,
    MAX_PRIORITY
}
