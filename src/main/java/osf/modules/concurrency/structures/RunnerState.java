package osf.modules.concurrency.structures;

public enum RunnerState {
    WAITING,
    EXECUTING,
}
