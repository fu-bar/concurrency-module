package osf.modules.concurrency.structures;

public enum ExecutionState {
    AWAITING,
    RUNNING,
    FINISHED
}
