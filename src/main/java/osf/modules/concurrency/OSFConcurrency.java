package osf.modules.concurrency;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.modules.concurrency.structures.OSFTask;
import osf.modules.concurrency.structures.RunnerState;
import osf.modules.configuration.interfaces.ConfProviderInterface;
import osf.shared.annotations.InjectService;
import osf.shared.annotations.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

@Singleton
@InjectService
public class OSFConcurrency {
    private final BlockingQueue<OSFTask> osfTasks = new LinkedBlockingQueue<>();
    private final Logger log = LogManager.getLogger(OSFConcurrency.class);
    private final List<OSFTaskRunner> taskRunners = new ArrayList<>();
    private final int maxIdleRunnerSeconds;

    public OSFConcurrency(ConfProviderInterface config) {
        this.maxIdleRunnerSeconds = config.get("THREADING::maxIdleRunnerSeconds").asInt();
    }

    public void runConcurrently(OSFTask task) {
        this.purgeIdleRunners();
        log.debug("Adding task: {} into concurrent execution queue", task.getName());
        if (taskRunners.isEmpty() || taskRunners.stream().allMatch(
                runner -> runner.getRunnerState() == RunnerState.EXECUTING)
        ) {
            OSFTaskRunner osfTaskRunner = new OSFTaskRunner("TaskRunner-" + taskRunners.size() + 1, this.osfTasks);
            osfTaskRunner.start();
            this.taskRunners.add(osfTaskRunner);
        }
        osfTasks.add(task);
    }

    private void purgeIdleRunners() {
        List<OSFTaskRunner> runnersToPurge = this.taskRunners.stream().filter(runner ->
                        runner.getIdleDuration().getSeconds() > maxIdleRunnerSeconds)
                .collect(Collectors.toList());
        runnersToPurge.forEach(runner -> {
            log.debug("Runner {} is idle and thus stopped and purged", runner.getRunnerID());
            runner.stop();
        });

        this.taskRunners.removeAll(runnersToPurge);
    }

    public int countActiveTaskRunners() {
        return this.taskRunners.size();
    }

    public void stop() {
        for (var taskRunner : this.taskRunners) {
            taskRunner.stop();
        }
    }
}
