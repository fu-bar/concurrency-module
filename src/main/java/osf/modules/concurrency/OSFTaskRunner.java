package osf.modules.concurrency;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.modules.concurrency.structures.OSFTask;
import osf.modules.concurrency.structures.RunnerState;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.BlockingQueue;

class OSFTaskRunner {
    private final BlockingQueue<OSFTask> osfTasks;
    private Thread thread;
    private final Logger log;
    private final String runnerID;
    private OSFTask currentTask;
    private RunnerState runnerState = RunnerState.WAITING;
    private Instant lastExecutionStartTimestamp = Instant.now();
    private Instant lastExecutionFinishTimestamp = Instant.now();

    public OSFTaskRunner(String runnerID, BlockingQueue<OSFTask> osfTasks) {
        this.runnerID = runnerID;
        this.log = LogManager.getLogger("Task-Runner: " + runnerID);
        this.osfTasks = osfTasks;
    }


    public void start() {
        log.info("Runner {} starting pulling tasks for concurrent execution", runnerID);
        this.thread = new Thread(this::pullTasksForExecution);
        this.thread.setDaemon(true);
        this.thread.start();
    }

    private void pullTasksForExecution() {
        while (!this.thread.isInterrupted()) {
            try {
                this.runnerState = RunnerState.WAITING;
                currentTask = this.osfTasks.take();
                this.runnerState = RunnerState.EXECUTING;
                this.lastExecutionStartTimestamp = Instant.now();

                log.debug("Executing task {}", currentTask.getName());
                Thread.currentThread().setName(currentTask.getName());
                currentTask.execute();
                this.lastExecutionFinishTimestamp = Instant.now();
            } catch (InterruptedException e) {
                this.lastExecutionFinishTimestamp = Instant.now();
                log.info("Runner {} thread interrupted - finishing", runnerID);
                Thread.currentThread().interrupt();
                return;
            }
        }
        log.info("Runner {} finished executing", runnerID);
    }

    public void stop() {
        log.info("Runner {} is stopping", runnerID);
        this.thread.interrupt();
    }

    public RunnerState getRunnerState() {
        return runnerState;
    }

    public Duration getExecutionDuration() {
        if (this.runnerState == RunnerState.WAITING) {
            return Duration.ZERO;
        }
        return Duration.between(this.lastExecutionStartTimestamp, Instant.now());
    }

    public Duration getIdleDuration() {
        if (this.runnerState == RunnerState.EXECUTING) {
            return Duration.ZERO;
        }
        return Duration.between(lastExecutionFinishTimestamp, Instant.now());
    }

    public String getRunnerID() {
        return runnerID;
    }
}
